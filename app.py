from flask import request, Flask

app = Flask(__name__)

@app.route('/')
def mainpage():
    return "<h1>Hello World!</h1>"

@app.route('/plus_one/<x>')
def plus_one(x):
    x = int(x)
    ans = x  +  1
    return f"{x} + 1 = {ans}"
    
@app.route('/square/<x>')
def square(x):
    x = int(x)
    ans = x * x
    return f"{x} ^ 2 = {ans}"

@app.route('/plus/<x>,<y>')
def plus(x,y):
    ans = int(x)  +  int(y)
    return f"{x} + {y} = {ans}"

